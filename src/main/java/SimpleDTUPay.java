import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SimpleDTUPay {
    Client client;
    WebTarget target;
    public SimpleDTUPay() {
        this.client = ClientBuilder.newClient();
        this.target = this.client.target("http://fm-11.compute.dtu.dk:8080/");
    }

    public Boolean pay(int amount, String cid, String mid) {
        WebTarget path = target.path("payments")
                .queryParam("amount",amount)
                .queryParam("cid", cid)
                .queryParam("mid", mid);
        Boolean result = path
                .request()
                .post(Entity.entity(false, MediaType.TEXT_PLAIN), Boolean.class);

        return result;
    }

    public String getAccount(String firstName, String lastName, String cprNumber, int balance){
        WebTarget path = target.path("account")
                .queryParam("firstName", firstName)
                .queryParam("lastName", lastName)
                .queryParam("cprNumber", cprNumber)
                .queryParam("balance", balance);
        String result = path
                .request()
                .accept(MediaType.TEXT_PLAIN)
                .get(String.class);

        return result;
    }

    public String createAccount(String accountID) {

        WebTarget path = target.path("account");
        String result = path
                .request()
                .post(Entity.entity(accountID, MediaType.TEXT_PLAIN), String.class);

        return result;
    }

    public void retireAccount(String accountID) {
        String var = "";
        WebTarget path = target.path("account")
                .queryParam("accountid", accountID);
        path.request().put(Entity.entity(var,MediaType.TEXT_PLAIN));
    }
}