public class Customer {
    String id;

    public Customer() {}

    public Customer(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }
}
