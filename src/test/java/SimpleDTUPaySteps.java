import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class SimpleDTUPaySteps {
    Customer customer = new Customer();
    Customer merchant =new Customer();
    SimpleDTUPay dtuPay = new SimpleDTUPay();
    Boolean successfulPayment = false;
    Boolean successfulPayment2;
    String id;
    String id2;

    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(Integer balance) {
        id = dtuPay.getAccount("Peter","Pjerson","586123-9537",balance);
    }

    @Given("that the customer is registered with DTU Pay")
    public void that_the_customer_is_registered_with_dtu_pay() {
        customer.setID(dtuPay.createAccount(id));
    }

    @Given("a merchant with a bank account with balance {int}")
    public void a_merchant_with_a_bank_account_with_balance(Integer balance) {
        id = dtuPay.getAccount("Anders", "Ryanson","071280-6110", balance);
    }

    @Given("that the merchant is registered with DTU Pay")
    public void that_the_merchant_is_registered_with_dtu_pay() {
        merchant.setID(dtuPay.createAccount(id));
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) {
        successfulPayment = dtuPay.pay(amount, customer.getID(), merchant.getID());
    }

    @Then("the payment is successful")
    public void the_payment_is_successful() {
        assertTrue(successfulPayment);
    }

    @Then("the payment is unsuccessful")
    public void thePaymentIsUnsuccessful() {
        assertFalse(successfulPayment);

    }

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void theCustomerWithCPRHasABankAccount(String firstName, String lastName, String cpr) {
        id = dtuPay.getAccount(firstName, lastName, cpr, 0);
    }

    @When("user gets registered in DTU-pay")
    public void userGetsRegisteredInDTUPay() {
        customer = new Customer(dtuPay.createAccount(id));
    }

    @After
    public void retireAccount(){
        if (customer != null){
            dtuPay.retireAccount(customer.getID());
        }
        if(merchant != null){
            dtuPay.retireAccount(merchant.getID());
        }
    }
}
